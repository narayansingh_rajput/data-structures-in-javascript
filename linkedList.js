/*
    LinkedList with 2 pointers pointing ot head and tail
*/
//creating linklist
function LinkedList() {
    this.head = null;
    this.tail = null;
}

//creating node
function Node(val, next, prev) {
    this.value = val;
    this.next = next;
    this.prev = prev;
}

//Adding node to head-----Constant runtime - Big O Notation:  "O (1)"
LinkedList.prototype.addToHead = function(val) {
    var newNode = new Node(val, this.head, null);
    if (this.head) this.head.prev = newNode;
    else this.tail = newNode;
    this.head = newNode;
};

//Adding node to tail----Constant runtime - Big O Notation:  "O (1)"
LinkedList.prototype.addToTail = function(val) {
    var newNode = new Node(val, null, this.tail);
    if (this.tail) this.tail.next = newNode;
    else this.head = newNode;
    this.tail = newNode;
};

//Removing Head node----Constant runtime - Big O Notation:  "O (1)"
LinkedList.prototype.removeHead = function(val) {
    if (!this.head) return null;
    var val = this.head.value;
    this.head = this.head.next;
    if (this.head) this.head.prev = null;
    else this.tail = null;
    return val;
};

//Removing tail node----Constant runtime - Big O Notation:  "O (1)"
LinkedList.prototype.removeTail = function() {
    if (!this.tail) return null;
    var val = this.tail.value;
    this.tail = this.tail.prev;
    if (this.tail) this.tail.next = null;
    else this.head = null;
    return val;
};

//Searching value in list-----Linear runtime - Big O Notation:  "O (n)"
LinkedList.prototype.search = function(key) {
    var currentNode = this.head;
    while (currentNode) {
        if (currentNode.value === key) {
            return currentNode.value;
        }
        currentNode = currentNode.next;
    }
    return null;
};

//Finding indexes of item in list-----Linear runtime - Big O Notation:  "O (n)"
LinkedList.prototype.indexOf = function(val) {
    var indexes = [];
    var currentNode = this.head;
    var i = 0;
    while (currentNode) {
        if (currentNode.value === val) {
            indexes.push(i);
        }
        i++;
        currentNode = currentNode.next;
    }
    return indexes;
};


var ll = new LinkedList();
